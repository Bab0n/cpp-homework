#include <iostream>
#include <cmath>

void third_task() {
    double V, a;
    std::cout << "Enter rib length: ";
    std::cin >> a;
    V = (5.0 / 12) * (3 + sqrt(5)) * (a * a * a);
    std::cout << "Value = " << V << std::endl;
}
